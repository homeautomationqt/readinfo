/*
 * Copyright 2012 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TEST_FRAME_PARSER_H
#define TEST_FRAME_PARSER_H

#include <QtCore/QObject>
#include <QtTest/QtTest>

class TestFrameParser: public QObject
{
    Q_OBJECT
public:

    TestFrameParser();

private Q_SLOTS:
    void nominalTest();

    void testHourTypeChanged();

    void testFullHoursCounterChanged();

    void testEmptyHoursCounterChanged();

    void testCurrentElectricCurrentChanged();

    void testMaximumElectricCurrentChanged();

    void testCurrentPowerUsageChanged();

    void testOverElectricPowerUsageChanged();

    void testCheckLineValidity();

};

#endif

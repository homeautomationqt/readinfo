/*
 * Copyright 2012 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "test_frame_parser.h"

#include "frame_parser.h"

#include <QtTest/QSignalSpy>

Q_DECLARE_METATYPE(QList<QByteArray>);

TestFrameParser::TestFrameParser()
{
    qRegisterMetaType<QList<QByteArray> >();
}

void TestFrameParser::nominalTest()
{
    FrameParser theFrameParser;

    QSignalSpy currentElectricCurrentChangedSpy(&theFrameParser, SIGNAL(currentElectricCurrentChanged()));
    QSignalSpy currentPowerUsageChangedSpy(&theFrameParser, SIGNAL(currentPowerUsageChanged()));
    QSignalSpy emptyHoursCounterChangedSpy(&theFrameParser, SIGNAL(emptyHoursCounterChanged()));
    QSignalSpy fullHoursCounterChangedSpy(&theFrameParser, SIGNAL(fullHoursCounterChanged()));
    QSignalSpy hourTypeChangedSpy(&theFrameParser, SIGNAL(hourTypeChanged()));
    QSignalSpy maximumElectricCurrentChangedSpy(&theFrameParser, SIGNAL(maximumElectricCurrentChanged()));
    QSignalSpy overElectricPowerUsageChangedSpy(&theFrameParser, SIGNAL(overElectricPowerUsageChanged()));

    QList<QByteArray> exampleFrame;
    exampleFrame.push_back("ADCO 049922076568 Q");
    exampleFrame.push_back("OPTARIF HC.. <");
    exampleFrame.push_back("ISOUSC 45 ?");
    exampleFrame.push_back("HCHC 061570362 $");
    exampleFrame.push_back("HCHP 062965203 4");
    exampleFrame.push_back("PTEC HC.. S");
    exampleFrame.push_back("IINST 010 X");
    exampleFrame.push_back("ADPS 049 E");
    exampleFrame.push_back("IMAX 049 L");
    exampleFrame.push_back("PAPP 02200 %");
    exampleFrame.push_back("HHPHC A ,");
    exampleFrame.push_back("MOTDETAT 000000 B");
    theFrameParser.newFrame(exampleFrame);

    QCOMPARE(currentElectricCurrentChangedSpy.count(), 1);
    QCOMPARE(currentPowerUsageChangedSpy.count(), 1);
    QCOMPARE(emptyHoursCounterChangedSpy.count(), 1);
    QCOMPARE(fullHoursCounterChangedSpy.count(), 1);
    QCOMPARE(hourTypeChangedSpy.count(), 1);
    QCOMPARE(maximumElectricCurrentChangedSpy.count(), 1);
    QCOMPARE(overElectricPowerUsageChangedSpy.count(), 1);
}

void TestFrameParser::testHourTypeChanged()
{
    FrameParser theFrameParser;

    QList<QByteArray> exampleFrame;
    exampleFrame.push_back("ADCO 049922076568 Q");
    exampleFrame.push_back("OPTARIF HC.. <");
    exampleFrame.push_back("ISOUSC 45 ?");
    exampleFrame.push_back("HCHC 061570362 $");
    exampleFrame.push_back("HCHP 062965203 4");
    exampleFrame.push_back("PTEC HC.. S");
    exampleFrame.push_back("IINST 010 X");
    exampleFrame.push_back("ADPS 049 E");
    exampleFrame.push_back("IMAX 049 L");
    exampleFrame.push_back("PAPP 02200 %");
    exampleFrame.push_back("HHPHC A ,");
    exampleFrame.push_back("MOTDETAT 000000 B");
    theFrameParser.newFrame(exampleFrame);

    QSignalSpy currentElectricCurrentChangedSpy(&theFrameParser, SIGNAL(currentElectricCurrentChanged()));
    QSignalSpy currentPowerUsageChangedSpy(&theFrameParser, SIGNAL(currentPowerUsageChanged()));
    QSignalSpy emptyHoursCounterChangedSpy(&theFrameParser, SIGNAL(emptyHoursCounterChanged()));
    QSignalSpy fullHoursCounterChangedSpy(&theFrameParser, SIGNAL(fullHoursCounterChanged()));
    QSignalSpy hourTypeChangedSpy(&theFrameParser, SIGNAL(hourTypeChanged()));
    QSignalSpy maximumElectricCurrentChangedSpy(&theFrameParser, SIGNAL(maximumElectricCurrentChanged()));
    QSignalSpy overElectricPowerUsageChangedSpy(&theFrameParser, SIGNAL(overElectricPowerUsageChanged()));

    QList<QByteArray> modifiedFrame;
    modifiedFrame.push_back("ADCO 049922076568 Q");
    modifiedFrame.push_back("OPTARIF HC.. <");
    modifiedFrame.push_back("ISOUSC 45 ?");
    modifiedFrame.push_back("HCHC 061570362 $");
    modifiedFrame.push_back("HCHP 062965203 4");
    modifiedFrame.push_back("PTEC HP..  ");
    modifiedFrame.push_back("IINST 010 X");
    modifiedFrame.push_back("ADPS 049 E");
    modifiedFrame.push_back("IMAX 049 L");
    modifiedFrame.push_back("PAPP 02200 %");
    modifiedFrame.push_back("HHPHC A ,");
    modifiedFrame.push_back("MOTDETAT 000000 B");
    theFrameParser.newFrame(modifiedFrame);

    QCOMPARE(currentElectricCurrentChangedSpy.count(), 0);
    QCOMPARE(currentPowerUsageChangedSpy.count(), 0);
    QCOMPARE(emptyHoursCounterChangedSpy.count(), 0);
    QCOMPARE(fullHoursCounterChangedSpy.count(), 0);
    QCOMPARE(hourTypeChangedSpy.count(), 1);
    QCOMPARE(maximumElectricCurrentChangedSpy.count(), 0);
    QCOMPARE(overElectricPowerUsageChangedSpy.count(), 0);
}

void TestFrameParser::testFullHoursCounterChanged()
{
    FrameParser theFrameParser;

    QList<QByteArray> exampleFrame;
    exampleFrame.push_back("ADCO 049922076568 Q");
    exampleFrame.push_back("OPTARIF HC.. <");
    exampleFrame.push_back("ISOUSC 45 ?");
    exampleFrame.push_back("HCHC 061570362 $");
    exampleFrame.push_back("HCHP 062965203 4");
    exampleFrame.push_back("PTEC HC.. S");
    exampleFrame.push_back("IINST 010 X");
    exampleFrame.push_back("ADPS 049 E");
    exampleFrame.push_back("IMAX 049 L");
    exampleFrame.push_back("PAPP 02200 %");
    exampleFrame.push_back("HHPHC A ,");
    exampleFrame.push_back("MOTDETAT 000000 B");
    theFrameParser.newFrame(exampleFrame);

    QSignalSpy currentElectricCurrentChangedSpy(&theFrameParser, SIGNAL(currentElectricCurrentChanged()));
    QSignalSpy currentPowerUsageChangedSpy(&theFrameParser, SIGNAL(currentPowerUsageChanged()));
    QSignalSpy emptyHoursCounterChangedSpy(&theFrameParser, SIGNAL(emptyHoursCounterChanged()));
    QSignalSpy fullHoursCounterChangedSpy(&theFrameParser, SIGNAL(fullHoursCounterChanged()));
    QSignalSpy hourTypeChangedSpy(&theFrameParser, SIGNAL(hourTypeChanged()));
    QSignalSpy maximumElectricCurrentChangedSpy(&theFrameParser, SIGNAL(maximumElectricCurrentChanged()));
    QSignalSpy overElectricPowerUsageChangedSpy(&theFrameParser, SIGNAL(overElectricPowerUsageChanged()));

    QList<QByteArray> modifiedFrame;
    modifiedFrame.push_back("ADCO 049922076568 Q");
    modifiedFrame.push_back("OPTARIF HC.. <");
    modifiedFrame.push_back("ISOUSC 45 ?");
    modifiedFrame.push_back("HCHC 061570362 $");
    modifiedFrame.push_back("HCHP 062965204 5");
    modifiedFrame.push_back("PTEC HC.. S");
    modifiedFrame.push_back("IINST 010 X");
    modifiedFrame.push_back("ADPS 049 E");
    modifiedFrame.push_back("IMAX 049 L");
    modifiedFrame.push_back("PAPP 02200 %");
    modifiedFrame.push_back("HHPHC A ,");
    modifiedFrame.push_back("MOTDETAT 000000 B");
    theFrameParser.newFrame(modifiedFrame);

    QCOMPARE(currentElectricCurrentChangedSpy.count(), 0);
    QCOMPARE(currentPowerUsageChangedSpy.count(), 0);
    QCOMPARE(emptyHoursCounterChangedSpy.count(), 0);
    QCOMPARE(fullHoursCounterChangedSpy.count(), 1);
    QCOMPARE(hourTypeChangedSpy.count(), 0);
    QCOMPARE(maximumElectricCurrentChangedSpy.count(), 0);
    QCOMPARE(overElectricPowerUsageChangedSpy.count(), 0);
}

void TestFrameParser::testEmptyHoursCounterChanged()
{
    FrameParser theFrameParser;

    QList<QByteArray> exampleFrame;
    exampleFrame.push_back("ADCO 049922076568 Q");
    exampleFrame.push_back("OPTARIF HC.. <");
    exampleFrame.push_back("ISOUSC 45 ?");
    exampleFrame.push_back("HCHC 061570362 $");
    exampleFrame.push_back("HCHP 062965203 4");
    exampleFrame.push_back("PTEC HC.. S");
    exampleFrame.push_back("IINST 010 X");
    exampleFrame.push_back("ADPS 049 E");
    exampleFrame.push_back("IMAX 049 L");
    exampleFrame.push_back("PAPP 02200 %");
    exampleFrame.push_back("HHPHC A ,");
    exampleFrame.push_back("MOTDETAT 000000 B");
    theFrameParser.newFrame(exampleFrame);

    QSignalSpy currentElectricCurrentChangedSpy(&theFrameParser, SIGNAL(currentElectricCurrentChanged()));
    QSignalSpy currentPowerUsageChangedSpy(&theFrameParser, SIGNAL(currentPowerUsageChanged()));
    QSignalSpy emptyHoursCounterChangedSpy(&theFrameParser, SIGNAL(emptyHoursCounterChanged()));
    QSignalSpy fullHoursCounterChangedSpy(&theFrameParser, SIGNAL(fullHoursCounterChanged()));
    QSignalSpy hourTypeChangedSpy(&theFrameParser, SIGNAL(hourTypeChanged()));
    QSignalSpy maximumElectricCurrentChangedSpy(&theFrameParser, SIGNAL(maximumElectricCurrentChanged()));
    QSignalSpy overElectricPowerUsageChangedSpy(&theFrameParser, SIGNAL(overElectricPowerUsageChanged()));

    QList<QByteArray> modifiedFrame;
    modifiedFrame.push_back("ADCO 049922076568 Q");
    modifiedFrame.push_back("OPTARIF HC.. <");
    modifiedFrame.push_back("ISOUSC 45 ?");
    modifiedFrame.push_back("HCHC 061570363 %");
    modifiedFrame.push_back("HCHP 062965203 4");
    modifiedFrame.push_back("PTEC HC.. S");
    modifiedFrame.push_back("IINST 010 X");
    modifiedFrame.push_back("ADPS 049 E");
    modifiedFrame.push_back("IMAX 049 L");
    modifiedFrame.push_back("PAPP 02200 %");
    modifiedFrame.push_back("HHPHC A ,");
    modifiedFrame.push_back("MOTDETAT 000000 B");
    theFrameParser.newFrame(modifiedFrame);

    QCOMPARE(currentElectricCurrentChangedSpy.count(), 0);
    QCOMPARE(currentPowerUsageChangedSpy.count(), 0);
    QCOMPARE(emptyHoursCounterChangedSpy.count(), 1);
    QCOMPARE(fullHoursCounterChangedSpy.count(), 0);
    QCOMPARE(hourTypeChangedSpy.count(), 0);
    QCOMPARE(maximumElectricCurrentChangedSpy.count(), 0);
    QCOMPARE(overElectricPowerUsageChangedSpy.count(), 0);
}

void TestFrameParser::testCurrentElectricCurrentChanged()
{
    FrameParser theFrameParser;

    QList<QByteArray> exampleFrame;
    exampleFrame.push_back("ADCO 049922076568 Q");
    exampleFrame.push_back("OPTARIF HC.. <");
    exampleFrame.push_back("ISOUSC 45 ?");
    exampleFrame.push_back("HCHC 061570362 $");
    exampleFrame.push_back("HCHP 062965203 4");
    exampleFrame.push_back("PTEC HC.. S");
    exampleFrame.push_back("IINST 010 X");
    exampleFrame.push_back("ADPS 049 E");
    exampleFrame.push_back("IMAX 049 L");
    exampleFrame.push_back("PAPP 02200 %");
    exampleFrame.push_back("HHPHC A ,");
    exampleFrame.push_back("MOTDETAT 000000 B");
    theFrameParser.newFrame(exampleFrame);

    QSignalSpy currentElectricCurrentChangedSpy(&theFrameParser, SIGNAL(currentElectricCurrentChanged()));
    QSignalSpy currentPowerUsageChangedSpy(&theFrameParser, SIGNAL(currentPowerUsageChanged()));
    QSignalSpy emptyHoursCounterChangedSpy(&theFrameParser, SIGNAL(emptyHoursCounterChanged()));
    QSignalSpy fullHoursCounterChangedSpy(&theFrameParser, SIGNAL(fullHoursCounterChanged()));
    QSignalSpy hourTypeChangedSpy(&theFrameParser, SIGNAL(hourTypeChanged()));
    QSignalSpy maximumElectricCurrentChangedSpy(&theFrameParser, SIGNAL(maximumElectricCurrentChanged()));
    QSignalSpy overElectricPowerUsageChangedSpy(&theFrameParser, SIGNAL(overElectricPowerUsageChanged()));

    QList<QByteArray> modifiedFrame;
    modifiedFrame.push_back("ADCO 049922076568 Q");
    modifiedFrame.push_back("OPTARIF HC.. <");
    modifiedFrame.push_back("ISOUSC 45 ?");
    modifiedFrame.push_back("HCHC 061570362 $");
    modifiedFrame.push_back("HCHP 062965203 4");
    modifiedFrame.push_back("PTEC HC.. S");
    modifiedFrame.push_back("IINST 011 Y");
    modifiedFrame.push_back("ADPS 049 E");
    modifiedFrame.push_back("IMAX 049 L");
    modifiedFrame.push_back("PAPP 02200 %");
    modifiedFrame.push_back("HHPHC A ,");
    modifiedFrame.push_back("MOTDETAT 000000 B");
    theFrameParser.newFrame(modifiedFrame);

    QCOMPARE(currentElectricCurrentChangedSpy.count(), 1);
    QCOMPARE(currentPowerUsageChangedSpy.count(), 0);
    QCOMPARE(emptyHoursCounterChangedSpy.count(), 0);
    QCOMPARE(fullHoursCounterChangedSpy.count(), 0);
    QCOMPARE(hourTypeChangedSpy.count(), 0);
    QCOMPARE(maximumElectricCurrentChangedSpy.count(), 0);
    QCOMPARE(overElectricPowerUsageChangedSpy.count(), 0);
}

void TestFrameParser::testMaximumElectricCurrentChanged()
{
    FrameParser theFrameParser;

    QList<QByteArray> exampleFrame;
    exampleFrame.push_back("ADCO 049922076568 Q");
    exampleFrame.push_back("OPTARIF HC.. <");
    exampleFrame.push_back("ISOUSC 45 ?");
    exampleFrame.push_back("HCHC 061570362 $");
    exampleFrame.push_back("HCHP 062965203 4");
    exampleFrame.push_back("PTEC HC.. S");
    exampleFrame.push_back("IINST 010 X");
    exampleFrame.push_back("ADPS 049 E");
    exampleFrame.push_back("IMAX 049 L");
    exampleFrame.push_back("PAPP 02200 %");
    exampleFrame.push_back("HHPHC A ,");
    exampleFrame.push_back("MOTDETAT 000000 B");
    theFrameParser.newFrame(exampleFrame);

    QSignalSpy currentElectricCurrentChangedSpy(&theFrameParser, SIGNAL(currentElectricCurrentChanged()));
    QSignalSpy currentPowerUsageChangedSpy(&theFrameParser, SIGNAL(currentPowerUsageChanged()));
    QSignalSpy emptyHoursCounterChangedSpy(&theFrameParser, SIGNAL(emptyHoursCounterChanged()));
    QSignalSpy fullHoursCounterChangedSpy(&theFrameParser, SIGNAL(fullHoursCounterChanged()));
    QSignalSpy hourTypeChangedSpy(&theFrameParser, SIGNAL(hourTypeChanged()));
    QSignalSpy maximumElectricCurrentChangedSpy(&theFrameParser, SIGNAL(maximumElectricCurrentChanged()));
    QSignalSpy overElectricPowerUsageChangedSpy(&theFrameParser, SIGNAL(overElectricPowerUsageChanged()));

    QList<QByteArray> modifiedFrame;
    modifiedFrame.push_back("ADCO 049922076568 Q");
    modifiedFrame.push_back("OPTARIF HC.. <");
    modifiedFrame.push_back("ISOUSC 45 ?");
    modifiedFrame.push_back("HCHC 061570362 $");
    modifiedFrame.push_back("HCHP 062965203 4");
    modifiedFrame.push_back("PTEC HC.. S");
    modifiedFrame.push_back("IINST 010 X");
    modifiedFrame.push_back("ADPS 049 E");
    modifiedFrame.push_back("IMAX 052 F");
    modifiedFrame.push_back("PAPP 02200 %");
    modifiedFrame.push_back("HHPHC A ,");
    modifiedFrame.push_back("MOTDETAT 000000 B");
    theFrameParser.newFrame(modifiedFrame);

    QCOMPARE(currentElectricCurrentChangedSpy.count(), 0);
    QCOMPARE(currentPowerUsageChangedSpy.count(), 0);
    QCOMPARE(emptyHoursCounterChangedSpy.count(), 0);
    QCOMPARE(fullHoursCounterChangedSpy.count(), 0);
    QCOMPARE(hourTypeChangedSpy.count(), 0);
    QCOMPARE(maximumElectricCurrentChangedSpy.count(), 1);
    QCOMPARE(overElectricPowerUsageChangedSpy.count(), 0);
}

void TestFrameParser::testCurrentPowerUsageChanged()
{
    FrameParser theFrameParser;

    QList<QByteArray> exampleFrame;
    exampleFrame.push_back("ADCO 049922076568 Q");
    exampleFrame.push_back("OPTARIF HC.. <");
    exampleFrame.push_back("ISOUSC 45 ?");
    exampleFrame.push_back("HCHC 061570362 $");
    exampleFrame.push_back("HCHP 062965203 4");
    exampleFrame.push_back("PTEC HC.. S");
    exampleFrame.push_back("IINST 010 X");
    exampleFrame.push_back("ADPS 049 E");
    exampleFrame.push_back("IMAX 049 L");
    exampleFrame.push_back("PAPP 02200 %");
    exampleFrame.push_back("HHPHC A ,");
    exampleFrame.push_back("MOTDETAT 000000 B");
    theFrameParser.newFrame(exampleFrame);

    QSignalSpy currentElectricCurrentChangedSpy(&theFrameParser, SIGNAL(currentElectricCurrentChanged()));
    QSignalSpy currentPowerUsageChangedSpy(&theFrameParser, SIGNAL(currentPowerUsageChanged()));
    QSignalSpy emptyHoursCounterChangedSpy(&theFrameParser, SIGNAL(emptyHoursCounterChanged()));
    QSignalSpy fullHoursCounterChangedSpy(&theFrameParser, SIGNAL(fullHoursCounterChanged()));
    QSignalSpy hourTypeChangedSpy(&theFrameParser, SIGNAL(hourTypeChanged()));
    QSignalSpy maximumElectricCurrentChangedSpy(&theFrameParser, SIGNAL(maximumElectricCurrentChanged()));
    QSignalSpy overElectricPowerUsageChangedSpy(&theFrameParser, SIGNAL(overElectricPowerUsageChanged()));

    QList<QByteArray> modifiedFrame;
    modifiedFrame.push_back("ADCO 049922076568 Q");
    modifiedFrame.push_back("OPTARIF HC.. <");
    modifiedFrame.push_back("ISOUSC 45 ?");
    modifiedFrame.push_back("HCHC 061570362 $");
    modifiedFrame.push_back("HCHP 062965203 4");
    modifiedFrame.push_back("PTEC HC.. S");
    modifiedFrame.push_back("IINST 010 X");
    modifiedFrame.push_back("ADPS 049 E");
    modifiedFrame.push_back("IMAX 049 L");
    modifiedFrame.push_back("PAPP 02500 (");
    modifiedFrame.push_back("HHPHC A ,");
    modifiedFrame.push_back("MOTDETAT 000000 B");
    theFrameParser.newFrame(modifiedFrame);

    QCOMPARE(currentElectricCurrentChangedSpy.count(), 0);
    QCOMPARE(currentPowerUsageChangedSpy.count(), 1);
    QCOMPARE(emptyHoursCounterChangedSpy.count(), 0);
    QCOMPARE(fullHoursCounterChangedSpy.count(), 0);
    QCOMPARE(hourTypeChangedSpy.count(), 0);
    QCOMPARE(maximumElectricCurrentChangedSpy.count(), 0);
    QCOMPARE(overElectricPowerUsageChangedSpy.count(), 0);
}

void TestFrameParser::testOverElectricPowerUsageChanged()
{
    FrameParser theFrameParser;

    QList<QByteArray> exampleFrame;
    exampleFrame.push_back("ADCO 049922076568 Q");
    exampleFrame.push_back("OPTARIF HC.. <");
    exampleFrame.push_back("ISOUSC 45 ?");
    exampleFrame.push_back("HCHC 061570362 $");
    exampleFrame.push_back("HCHP 062965203 4");
    exampleFrame.push_back("PTEC HC.. S");
    exampleFrame.push_back("IINST 010 X");
    exampleFrame.push_back("ADPS 049 E");
    exampleFrame.push_back("IMAX 049 L");
    exampleFrame.push_back("PAPP 02200 %");
    exampleFrame.push_back("HHPHC A ,");
    exampleFrame.push_back("MOTDETAT 000000 B");
    theFrameParser.newFrame(exampleFrame);

    QSignalSpy currentElectricCurrentChangedSpy(&theFrameParser, SIGNAL(currentElectricCurrentChanged()));
    QSignalSpy currentPowerUsageChangedSpy(&theFrameParser, SIGNAL(currentPowerUsageChanged()));
    QSignalSpy emptyHoursCounterChangedSpy(&theFrameParser, SIGNAL(emptyHoursCounterChanged()));
    QSignalSpy fullHoursCounterChangedSpy(&theFrameParser, SIGNAL(fullHoursCounterChanged()));
    QSignalSpy hourTypeChangedSpy(&theFrameParser, SIGNAL(hourTypeChanged()));
    QSignalSpy maximumElectricCurrentChangedSpy(&theFrameParser, SIGNAL(maximumElectricCurrentChanged()));
    QSignalSpy overElectricPowerUsageChangedSpy(&theFrameParser, SIGNAL(overElectricPowerUsageChanged()));

    QList<QByteArray> modifiedFrame;
    modifiedFrame.push_back("ADCO 049922076568 Q");
    modifiedFrame.push_back("OPTARIF HC.. <");
    modifiedFrame.push_back("ISOUSC 45 ?");
    modifiedFrame.push_back("HCHC 061570362 $");
    modifiedFrame.push_back("HCHP 062965203 4");
    modifiedFrame.push_back("PTEC HC.. S");
    modifiedFrame.push_back("IINST 010 X");
    modifiedFrame.push_back("IMAX 049 L");
    modifiedFrame.push_back("PAPP 02200 %");
    modifiedFrame.push_back("HHPHC A ,");
    modifiedFrame.push_back("MOTDETAT 000000 B");
    theFrameParser.newFrame(modifiedFrame);

    QCOMPARE(currentElectricCurrentChangedSpy.count(), 0);
    QCOMPARE(currentPowerUsageChangedSpy.count(), 0);
    QCOMPARE(emptyHoursCounterChangedSpy.count(), 0);
    QCOMPARE(fullHoursCounterChangedSpy.count(), 0);
    QCOMPARE(hourTypeChangedSpy.count(), 0);
    QCOMPARE(maximumElectricCurrentChangedSpy.count(), 0);
    QCOMPARE(overElectricPowerUsageChangedSpy.count(), 1);
}

void TestFrameParser::testCheckLineValidity()
{
    FrameParser theFrameParser;

    QList<QByteArray> exampleFrame;
    exampleFrame.push_back("ADCO 049922076568 Q");
    exampleFrame.push_back("OPTARIF HC.. <");
    exampleFrame.push_back("ISOUSC 45 ?");
    exampleFrame.push_back("HCHC 061570362 $");
    exampleFrame.push_back("HCHP 062965203 4");
    exampleFrame.push_back("PTEC HC.. S");
    exampleFrame.push_back("IINST 010 X");
    exampleFrame.push_back("ADPS 049 E");
    exampleFrame.push_back("IMAX 049 L");
    exampleFrame.push_back("PAPP 02200 %");
    exampleFrame.push_back("HHPHC A ,");
    exampleFrame.push_back("MOTDETAT 000000 B");
    theFrameParser.newFrame(exampleFrame);

    QSignalSpy currentElectricCurrentChangedSpy(&theFrameParser, SIGNAL(currentElectricCurrentChanged()));
    QSignalSpy currentPowerUsageChangedSpy(&theFrameParser, SIGNAL(currentPowerUsageChanged()));
    QSignalSpy emptyHoursCounterChangedSpy(&theFrameParser, SIGNAL(emptyHoursCounterChanged()));
    QSignalSpy fullHoursCounterChangedSpy(&theFrameParser, SIGNAL(fullHoursCounterChanged()));
    QSignalSpy hourTypeChangedSpy(&theFrameParser, SIGNAL(hourTypeChanged()));
    QSignalSpy maximumElectricCurrentChangedSpy(&theFrameParser, SIGNAL(maximumElectricCurrentChanged()));
    QSignalSpy overElectricPowerUsageChangedSpy(&theFrameParser, SIGNAL(overElectricPowerUsageChanged()));

    QList<QByteArray> modifiedFrame;
    modifiedFrame.push_back("ADCO 049922076568 Q");
    modifiedFrame.push_back("OPTARIF HC.. <");
    modifiedFrame.push_back("ISOUSC 45 ?");
    modifiedFrame.push_back("HCHC 061570362 $");
    modifiedFrame.push_back("HCHP 062965203 4");
    modifiedFrame.push_back("PTEC HC.. S");
    modifiedFrame.push_back("IINST 010 X");
    modifiedFrame.push_back("ADPS 049 E");
    modifiedFrame.push_back("IMAX 052 L");
    modifiedFrame.push_back("PAPP 02200 %");
    modifiedFrame.push_back("HHPHC A ,");
    modifiedFrame.push_back("MOTDETAT 000000 B");
    theFrameParser.newFrame(modifiedFrame);

    QCOMPARE(currentElectricCurrentChangedSpy.count(), 0);
    QCOMPARE(currentPowerUsageChangedSpy.count(), 0);
    QCOMPARE(emptyHoursCounterChangedSpy.count(), 0);
    QCOMPARE(fullHoursCounterChangedSpy.count(), 0);
    QCOMPARE(hourTypeChangedSpy.count(), 0);
    QCOMPARE(maximumElectricCurrentChangedSpy.count(), 0);
    QCOMPARE(overElectricPowerUsageChangedSpy.count(), 0);
}


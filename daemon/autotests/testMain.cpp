/*
 * Copyright 2012 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QtCore/QCoreApplication>
#include <QtTest/QTest>
#include "frame_grouper.h"
#include "test_frame_grouper.h"
#include "test_frame_parser.h"
#include "frame_parser.h"

int testFrameGrouper(int argc, char *argv[])
{
    TestFrameGrouper testObject;

    return QTest::qExec(&testObject, argc, argv);
}

int testFrameParser(int argc, char *argv[])
{
    TestFrameParser testObject;

    return QTest::qExec(&testObject, argc, argv);
}

int main(int argc, char *argv[])
{
    int result = 0;

    result = testFrameParser(argc, argv);

    return result;
}

/*
 * Copyright 2012 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "frame_exporter.h"
#include "exporter_adaptator.h"
#include "frame_parser.h"

#include <QtJolie/Server>
#include <QtJolie/Value>
#include <QtJolie/Message>

#include <QtCore/QDebug>
#include <QtCore/QMetaObject>
#include <QtCore/QMetaProperty>
#include <QtCore/QMap>
#include <QtCore/QHash>

class FrameExporterPrivate
{
public:

    FrameExporterPrivate(FrameParser *parser, FrameExporter *publicClass, quint16 port)
        : mServer(port), mAdaptator(publicClass), mExporter(parser), mChangeSignalToProperty()
    {
    }

    Jolie::Server mServer;
    ExporterAdaptor mAdaptator;
    QObject *mExporter;
    QMap<int, QByteArray> mChangeSignalToProperty;
    QHash<QByteArray, QMetaProperty> mProperties;
    Jolie::Value mPropertyList;
};

FrameExporter::FrameExporter(FrameParser *parser, quint16 port, QObject *parent)
    : QObject(parent), d(new FrameExporterPrivate(parser, this, port))
{
    bool res = d->mServer.registerAdaptor("/", &d->mAdaptator);
    registerAllPropertyChangedSignals();
}

const Jolie::Value& FrameExporter::propertyList() const
{
    return d->mPropertyList;
}

Jolie::Value FrameExporter::propertyValue(const QByteArray &name) const
{
    const QVariant &propertyValue = d->mExporter->property(name.constData());

    if (propertyValue.isValid()) {
        if (propertyValue.canConvert<int>()) {
            return Jolie::Value(propertyValue.toInt());
        }
        if (propertyValue.canConvert<double>()) {
            return Jolie::Value(propertyValue.toDouble());
        }
        if (propertyValue.canConvert<QByteArray>()) {
            return Jolie::Value(propertyValue.toByteArray());
        }
    }

    return Jolie::Value();
}

void FrameExporter::registerAllPropertyChangedSignals()
{
    QMetaMethod propertyChangedSignal = metaObject()->method(metaObject()->indexOfSlot("propertyChanged()"));
    const int nbProperties = d->mExporter->metaObject()->propertyCount();
    for (int i = 0; i < nbProperties; ++i) {
        const QMetaProperty &currentProperty = d->mExporter->metaObject()->property(i);
        if (currentProperty.hasNotifySignal()) {
            d->mProperties[d->mChangeSignalToProperty[currentProperty.notifySignalIndex()]] = currentProperty;
            connect(d->mExporter, currentProperty.notifySignal(), this, propertyChangedSignal);
            d->mPropertyList.children("properties").push_back(Jolie::Value(currentProperty.name()));
        }
    }
}

void FrameExporter::propertyChanged()
{
    const int senderSignal = senderSignalIndex();
    if (d->mChangeSignalToProperty.contains(senderSignal)) {
        QMetaProperty changedProperty = d->mProperties[d->mChangeSignalToProperty[senderSignal]];
        qDebug() << "changed property:" << changedProperty.name();
    }
}

bool FrameExporter::registerNotifierOnChange(const QByteArray &name, const Jolie::Value &currentValue, int clientId, qint64 messageId)
{
#if 0
    QMetaProperty theProperty = d->mProperties[name];
    if (propertyValue(name) == currentValue) {
    } else {
        Jolie::Message answerMsg("/", "notifyChange", messageId);

        answerMsg.setData(propertyValue(name));

        d->mServer.sendReply(clientId, answerMsg);
    }
#endif
    return false;
}

#include "frame_exporter.moc"

/*
 * Copyright 2012 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#if !defined FRAME_PARSER_H
#define FRAME_PARSER_H

#include <QtCore/QtGlobal>
#include <QtCore/QObject>
#include <QtCore/QList>
#include <QtCore/QByteArray>

class FrameParserPrivate;

class FrameParser : public QObject
{
    Q_OBJECT

    Q_CLASSINFO("D-Bus Interface", "org.mgallien.RemoteInformation")

    Q_PROPERTY(qint64 counterAddress READ counterAddress)

    Q_PROPERTY(int subscribdedMaximumCurrent READ subscribdedMaximumCurrent)

    Q_PROPERTY(bool isFullHours READ isFullHours NOTIFY hourTypeChanged)

    Q_PROPERTY(qint32 fullHoursCounter READ fullHoursCounter NOTIFY fullHoursCounterChanged)

    Q_PROPERTY(qint32 emptyHoursCounter READ emptyHoursCounter NOTIFY emptyHoursCounterChanged)

    Q_PROPERTY(qint32 currentElectricCurrent READ currentElectricCurrent NOTIFY currentElectricCurrentChanged)

    Q_PROPERTY(qint32 maximumElectricCurrent READ maximumElectricCurrent NOTIFY maximumElectricCurrentChanged)

    Q_PROPERTY(qint32 currentPowerUsage READ currentPowerUsage NOTIFY currentPowerUsageChanged)

    Q_PROPERTY(bool overElectricPowerUsage READ overElectricPowerUsage NOTIFY overElectricPowerUsageChanged)

public:

    FrameParser(QObject *parent = 0);

Q_SIGNALS:

    void hourTypeChanged();

    void fullHoursCounterChanged();

    void emptyHoursCounterChanged();

    void currentElectricCurrentChanged();

    void maximumElectricCurrentChanged();

    void currentPowerUsageChanged();

    void overElectricPowerUsageChanged();

public Q_SLOTS:

    void newFrame(const QList<QByteArray> &frame);

    qint64 counterAddress() const;

    int subscribdedMaximumCurrent() const;

    bool isFullHours() const;

    qint32 fullHoursCounter() const;

    qint32 emptyHoursCounter() const;

    qint32 currentElectricCurrent() const;

    qint32 maximumElectricCurrent() const;

    qint32 currentPowerUsage() const;

    char fullHoursScheduleType() const;

    bool overElectricPowerUsage() const;

private Q_SLOTS:

    void emitHourTypeChanged();

    void emitFullHoursCounterChanged();

    void emitEmptyHoursCounterChanged();

    void emitCurrentElectricCurrentChanged();

    void emitMaximumElectricCurrentChanged();

    void emitCurrentPowerUsageChanged();

    void emitOverElectricPowerUsageChanged();

private:

    bool checkLineValidity(QByteArray frameLine);

    FrameParserPrivate *d;
};

#endif

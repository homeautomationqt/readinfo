/*
 * Copyright 2012 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "frame_grouper.h"

#include <QtCore/QIODevice>
#include <QtCore/QBuffer>
#include <QtCore/QDebug>
#include <QtCore/QByteArrayMatcher>

class FrameGrouperPrivate
{
public:
    FrameGrouperPrivate() : serialPort(0), currentFrameGroup(),
                            adcoLine("ADCO"), motdetatLine("MOTDETAT")
    {
    }

    QIODevice *serialPort;
    QList<QByteArray> currentFrameGroup;
    bool newFrame;
    QByteArrayMatcher adcoLine;
    QByteArrayMatcher motdetatLine;
};

FrameGrouper::FrameGrouper(QObject *parent) : QObject(parent), d(new FrameGrouperPrivate)
{
}

void FrameGrouper::setSerialPort(QIODevice *port)
{
    if (d->serialPort) {
        disconnect(d->serialPort);
    }
    d->serialPort = port;
    connect(d->serialPort, SIGNAL(readyRead()), this, SLOT(serialPortReadyRead()));
}

const QIODevice* FrameGrouper::serialPort() const
{
    return d->serialPort;
}

void FrameGrouper::serialPortReadyRead()
{
    if (d->serialPort->canReadLine()) {
        QByteArray temp = d->serialPort->readLine();

        if (d->adcoLine.indexIn(temp) == 0) {
            temp.chop(2);
            d->currentFrameGroup.clear();
            d->currentFrameGroup.push_back(temp);
        } else {
            if (d->motdetatLine.indexIn(temp) == 0) {
                temp.chop(4);
                d->currentFrameGroup.push_back(temp);
                Q_EMIT newFrame(d->currentFrameGroup);
            } else {
                temp.chop(2);
                d->currentFrameGroup.push_back(temp);
            }
        }
    }
}

#include "frame_grouper.moc"

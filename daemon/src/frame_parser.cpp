/*
 * Copyright 2012 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "frame_parser.h"
#include "remoteinformationadaptor.h"

#include <QtCore/QtGlobal>
#include <QtCore/QObject>
#include <QtCore/QByteArrayMatcher>
#include <QtCore/QTimer>

class FrameParserPrivate
{
public:

    FrameParserPrivate() : counterAddress(0), subscribdedMaximumCurrent(0), isFullHours(true), fullHoursCounter(0),
        emptyHoursCounter(0), currentElectricCurrent(0), maximumElectricCurrent(0), currentPowerUsage(0),
        fullHoursScheduleType(' '), overElectricPowerUsage(false), adcoLine("ADCO"), optarifLine("OPTARIF"),
        isouscLine("ISOUSC"), hchcLine("HCHC"), hchpLine("HCHP"), ptecLine("PTEC"), iinstLine("IINST"),
        adpsLine("ADPS"), imaxLine("IMAX"), pappLine("PAPP"), hhphcLine("HHPHC"), motdetatLine("MOTDETAT"),
        patternFullHours("HP..")
    {
    }

    qulonglong counterAddress;

    int subscribdedMaximumCurrent;

    bool isFullHours;

    qint32 fullHoursCounter;

    qint32 emptyHoursCounter;

    qint32 currentElectricCurrent;

    qint32 maximumElectricCurrent;

    qint32 currentPowerUsage;

    char fullHoursScheduleType;

    bool overElectricPowerUsage;

    QByteArrayMatcher adcoLine;
    QByteArrayMatcher optarifLine;
    QByteArrayMatcher isouscLine;
    QByteArrayMatcher hchcLine;
    QByteArrayMatcher hchpLine;
    QByteArrayMatcher ptecLine;
    QByteArrayMatcher iinstLine;
    QByteArrayMatcher adpsLine;
    QByteArrayMatcher imaxLine;
    QByteArrayMatcher pappLine;
    QByteArrayMatcher hhphcLine;
    QByteArrayMatcher motdetatLine;

    QByteArray patternFullHours;
};

FrameParser::FrameParser(QObject *parent) : QObject(parent), d(new FrameParserPrivate)
{
    new RemoteInformationAdaptor(this);
    QDBusConnection dbus = QDBusConnection::systemBus();
    dbus.registerObject(QString::fromUtf8("/RemoteInformation"), this);
    dbus.registerService(QString::fromUtf8("org.mgallien.RemoteInformation"));
}

qint64 FrameParser::counterAddress() const
{
    return d->counterAddress;
}

int FrameParser::subscribdedMaximumCurrent() const
{
    return d->subscribdedMaximumCurrent;
}

bool FrameParser::isFullHours() const
{
    return d->isFullHours;
}

qint32 FrameParser::fullHoursCounter() const
{
    return d->fullHoursCounter;
}

qint32 FrameParser::emptyHoursCounter() const
{
    return d->emptyHoursCounter;
}

qint32 FrameParser::currentElectricCurrent() const
{
    return d->currentElectricCurrent;
}

qint32 FrameParser::maximumElectricCurrent() const
{
    return d->maximumElectricCurrent;
}

qint32 FrameParser::currentPowerUsage() const
{
    return d->currentPowerUsage;
}

char FrameParser::fullHoursScheduleType() const
{
    return d->fullHoursScheduleType;
}

bool FrameParser::overElectricPowerUsage() const
{
    return d->overElectricPowerUsage;
}

void FrameParser::newFrame(const QList<QByteArray> &frame)
{
    bool valid = true;

    for (QList<QByteArray>::const_iterator itLine = frame.begin(); valid && itLine != frame.end(); ++itLine) {
        valid &= checkLineValidity(*itLine);
    }

    bool overPowerDetected = false;
    for (QList<QByteArray>::const_iterator itLine = frame.begin(); valid && itLine != frame.end(); ++itLine) {
        if (d->adcoLine.indexIn(*itLine) == 0) {
            d->counterAddress = itLine->split(' ')[1].toULongLong();
        }

        if (d->isouscLine.indexIn(*itLine) == 0) {
            d->subscribdedMaximumCurrent = itLine->split(' ')[1].toInt();
        }

        if (d->hchpLine.indexIn(*itLine) == 0) {
            qint32 newValue = itLine->split(' ')[1].toInt();
            if (d->fullHoursCounter != newValue) {
                d->fullHoursCounter = newValue;
                QTimer::singleShot(2000, this, SLOT(emitFullHoursCounterChanged()));
            }
        }

        if (d->hchcLine.indexIn(*itLine) == 0) {
            qint32 newValue = itLine->split(' ')[1].toInt();
            if (d->emptyHoursCounter != newValue) {
                d->emptyHoursCounter = newValue;
                QTimer::singleShot(2000, this, SLOT(emitEmptyHoursCounterChanged()));
            }
        }

        if (d->ptecLine.indexIn(*itLine) == 0) {
            bool isDetectedFullHours = itLine->split(' ')[1] == d->patternFullHours;
            if (d->isFullHours != isDetectedFullHours) {
                d->isFullHours = isDetectedFullHours;
                QTimer::singleShot(2000, this, SLOT(emitHourTypeChanged()));
            }
        }

        if (d->iinstLine.indexIn(*itLine) == 0) {
            qint32 newValue = itLine->split(' ')[1].toInt();
            if (d->currentElectricCurrent != newValue) {
                d->currentElectricCurrent = newValue;
                QTimer::singleShot(2000, this, SLOT(emitCurrentElectricCurrentChanged()));
            }
        }

        if (d->adpsLine.indexIn(*itLine) == 0) {
            overPowerDetected = true;
        }

        if (d->imaxLine.indexIn(*itLine) == 0) {
            qint32 newValue = itLine->split(' ')[1].toInt();
            if (d->maximumElectricCurrent != newValue) {
                d->maximumElectricCurrent = newValue;
                QTimer::singleShot(2000, this, SLOT(emitMaximumElectricCurrentChanged()));
            }
        }

        if (d->pappLine.indexIn(*itLine) == 0) {
            qint32 newValue = itLine->split(' ')[1].toInt();
            if (d->currentPowerUsage != newValue) {
                d->currentPowerUsage = newValue;
                QTimer::singleShot(2000, this, SLOT(emitCurrentPowerUsageChanged()));
            }
        }

        if (d->hhphcLine.indexIn(*itLine) == 0) {
            d->fullHoursScheduleType = itLine->split(' ')[1][0];
        }
    }

    if (valid && d->overElectricPowerUsage != overPowerDetected) {
        d->overElectricPowerUsage = overPowerDetected;
        QTimer::singleShot(2000, this, SLOT(emitOverElectricPowerUsageChanged()));
    }
}

bool FrameParser::checkLineValidity(QByteArray frameLine)
{
    char lrc = 0;
    for (int i = 0; i < frameLine.size() - 2; ++i) {
        lrc += frameLine[i];
    }
    lrc = lrc & 0x3F;
    lrc += 0x20;
    const bool res = (lrc == frameLine[frameLine.size() - 1]);
    return res;
}

void FrameParser::emitHourTypeChanged()
{
    Q_EMIT hourTypeChanged();
}

void FrameParser::emitFullHoursCounterChanged()
{
    Q_EMIT fullHoursCounterChanged();
}

void FrameParser::emitEmptyHoursCounterChanged()
{
    Q_EMIT emptyHoursCounterChanged();
}

void FrameParser::emitCurrentElectricCurrentChanged()
{
    Q_EMIT currentElectricCurrentChanged();
}

void FrameParser::emitMaximumElectricCurrentChanged()
{
    Q_EMIT maximumElectricCurrentChanged();
}

void FrameParser::emitCurrentPowerUsageChanged()
{
    Q_EMIT currentPowerUsageChanged();
}

void FrameParser::emitOverElectricPowerUsageChanged()
{
    Q_EMIT overElectricPowerUsageChanged();
}

#include "frame_parser.moc"

/*
 * Copyright 2012 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "exporter_adaptator.h"
#include "frame_exporter.h"

#include <QtCore/QDebug>
#include <QtJolie/Message>
#include <QtJolie/Value>
#include <QtJolie/Server>

ExporterAdaptor::ExporterAdaptor(FrameExporter *exporter, QObject *parent) : Jolie::AbstractAdaptor(parent), mExporter(exporter)
{
}

void ExporterAdaptor::relay(Jolie::Server *server, int clientId, const Jolie::Message &message)
{
    if (message.operationName() == "properties") {
        Jolie::Message answerMsg(message.resourcePath(),
                                 message.operationName(),
                                 message.id());
        answerMsg.setData(mExporter->propertyList());
        server->sendReply(clientId, answerMsg);
        return;
    }

    if (message.operationName() == "getProperty") {
        Jolie::Message answerMsg(message.resourcePath(),
                                 message.operationName(),
                                 message.id());

        if (message.data().isValid() && message.data().isByteArray()) {
            const Jolie::Value &propertyValue(mExporter->propertyValue(message.data().toByteArray()));
            answerMsg.setData(propertyValue);
        }

        server->sendReply(clientId, answerMsg);
        return;
    }

    if (message.operationName() == "notifyChange") {
        Jolie::Message answerMsg(message.resourcePath(),
                                 message.operationName(),
                                 message.id());

        if (message.data().isValid() && message.data().isByteArray()) {
            if (message.data().children("currentValue").size() == 1 && message.data().children("currentValue").front().isValid()) {
                const Jolie::Value &currentValue = message.data().children("currentValue").front();
                mExporter->registerNotifierOnChange(message.data().toByteArray(), currentValue, clientId, message.id());
            }
        }

        server->sendReply(clientId, answerMsg);
        return;
    }

    Jolie::Message answerMsg(message.resourcePath(),
                             message.operationName(),
                             message.id());
    server->sendReply(clientId, answerMsg);
}

#include "exporter_adaptator.moc"

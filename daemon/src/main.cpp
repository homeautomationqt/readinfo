/*
 * Copyright 2012 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QtCore/QCoreApplication>
#include <QtCore/QDebug>
#include <qextserialport.h>
#include <qextserialenumerator.h>
#include "frame_grouper.h"
#include "frame_parser.h"
#include "frame_exporter.h"

int main(int argc, char *argv[])
{
    QCoreApplication theApp(argc, argv);

    PortSettings thePortSetting;
    thePortSetting.BaudRate = BAUD1200;
    thePortSetting.DataBits = DATA_7;
    thePortSetting.Parity = PAR_EVEN;
    thePortSetting.StopBits = STOP_1;
    thePortSetting.FlowControl = FLOW_OFF;

    QextSerialPort thePort(QString::fromUtf8("/dev/ttyS1"), thePortSetting);

    thePort.open(QIODevice::ReadOnly);

    FrameGrouper theGrouper;

    theGrouper.setSerialPort(&thePort);

    FrameParser theParser;

    theApp.connect(&theGrouper, SIGNAL(newFrame(QList<QByteArray>)), &theParser, SLOT(newFrame(QList<QByteArray>)));

    FrameExporter theExporter(&theParser, 1234);

    return theApp.exec();
}


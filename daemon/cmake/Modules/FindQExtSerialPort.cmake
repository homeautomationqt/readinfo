# - Try to find QExtSerialPort
# Once done, this will define
#
#  QExtSerialPort_FOUND - system has QExtSerialPort
#  QExtSerialPort_INCLUDE_DIRS - the QExtSerialPort include directories
#  QExtSerialPort_LIBRARIES - link these to use QExtSerialPort

include(LibFindMacros)

# Include dir
find_path(QExtSerialPort_INCLUDE_DIR
  NAMES qextserialport.h
  PATH_SUFFIXES QtExtSerialPort
)

# Finally the library itself
find_library(QExtSerialPort_LIBRARY
  NAMES qextserialport
)

# Set the include dir variables and the libraries and let libfind_process do the rest.
# NOTE: Singular variables for this library, plural for libraries this this lib depends on.
set(QExtSerialPort_PROCESS_INCLUDES QExtSerialPort_INCLUDE_DIR)
set(QExtSerialPort_PROCESS_LIBS QExtSerialPort_LIBRARY)
libfind_process(QExtSerialPort)


#sets the system to linux
set(CMAKE_SYSTEM_NAME Linux)

#sets the compiler to the path where it has been built with openembedded
set(CMAKE_C_COMPILER /var/local/oe/setup-scripts/build/tmp-angstrom_v2012_12-eglibc/sysroots/i686-linux/usr/bin/armv5te-angstrom-linux-gnueabi/arm-angstrom-linux-gnueabi-gcc)
set(CMAKE_CXX_COMPILER /var/local/oe/setup-scripts/build/tmp-angstrom_v2012_12-eglibc/sysroots/i686-linux/usr/bin/armv5te-angstrom-linux-gnueabi/arm-angstrom-linux-gnueabi-g++)

# sets the path where the target root system is available
set(CMAKE_FIND_ROOT_PATH /var/local/oe/setup-scripts/build/tmp-angstrom_v2012_12-eglibc/sysroots/netsam9x25)

set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)

set(QT_LIBINFIX E)
set(QT_HEADERS_DIR /var/local/oe/setup-scripts/build/tmp-angstrom_v2012_12-eglibc/sysroots/netsam9x25/usr/include/qtopia)
set(QT_QTCORE_INCLUDE_DIR ${QT_HEADERS_DIR}/QtCore)


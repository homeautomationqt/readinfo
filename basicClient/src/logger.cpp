/*
 * Copyright 2012 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "logger.h"

#include <QtCore/QDebug>
#include <QtCore/QFile>
#include <QtCore/QTextStream>
#include <QtCore/QProcess>
#include <QtCore/QLocale>

class LoggerPrivate
{
public:
    QObject *sourceObject;
};

Logger::Logger(QObject *sourceObject, QObject *parent) : QObject(parent), d(new LoggerPrivate)
{
    d->sourceObject = sourceObject;
    QLocale myLocale(QLocale::French, QLocale::France);
    myLocale.setNumberOptions(QLocale::OmitGroupSeparator);
    QLocale::setDefault(myLocale);
}
void Logger::updateRrd()
{
    QStringList args;
    args << QString::fromUtf8("update");
    args << QString::fromUtf8("/www/pages/electricStatistics.rrd");

    QString currentMeasure(QString::fromUtf8("N:%1:%2:%L3:%L4:%5"));
    currentMeasure = currentMeasure.arg(d->sourceObject->property("currentPowerUsage").toInt());
    currentMeasure = currentMeasure.arg(d->sourceObject->property("currentElectricCurrent").toInt());
    currentMeasure = currentMeasure.arg(d->sourceObject->property("emptyHoursCounter").toLongLong());
    currentMeasure = currentMeasure.arg(d->sourceObject->property("fullHoursCounter").toLongLong());
    currentMeasure = currentMeasure.arg(d->sourceObject->property("isFullHours").toBool());
    args << currentMeasure;
    qDebug() << currentMeasure;

    QProcess::execute(QString::fromUtf8("rrdtool"), args);
}

void Logger::hourTypeHasChanged()
{
    QFile outputFile(QString::fromUtf8("/www/pages/hourType.html"));
    QTextStream outputStream(&outputFile);
    outputFile.open(QFile::WriteOnly);

    outputStream << QString::fromUtf8("<!DOCTYPE html>\n<html>\n<head>\n");
    outputStream << QString::fromUtf8("<meta http-equiv=\"content-type\" content=\"text/html;charset=UTF-8\" />\n");
    outputStream << QString::fromUtf8("</head>\n<body>\n<h1>");
    outputStream << (d->sourceObject->property("isFullHours").toBool() ? QString::fromUtf8("Heures pleines") : QString::fromUtf8("Heures creuses"));
    outputStream << QString::fromUtf8("</h1>\n</body>\n</html>\n");
    updateRrd();
}

void Logger::fullHoursCounterHasChanged()
{
    QFile outputFile(QString::fromUtf8("/www/pages/fullHours.html"));
    QTextStream outputStream(&outputFile);
    outputFile.open(QFile::WriteOnly);

    outputStream << QString::fromUtf8("<!DOCTYPE html>\n<html>\n<head>\n");
    outputStream << QString::fromUtf8("<meta http-equiv=\"content-type\" content=\"text/html;charset=UTF-8\" />\n");
    outputStream << QString::fromUtf8("</head>\n<body>\n<h1>");
    outputStream << d->sourceObject->property("fullHoursCounter").toLongLong();
    outputStream << QString::fromUtf8("</h1>\n</body>\n</html>\n");
    updateRrd();
}

void Logger::emptyHoursCounterHasChanged()
{
    QFile outputFile(QString::fromUtf8("/www/pages/emptyHours.html"));
    QTextStream outputStream(&outputFile);
    outputFile.open(QFile::WriteOnly);

    outputStream << QString::fromUtf8("<!DOCTYPE html>\n<html>\n<head>\n");
    outputStream << QString::fromUtf8("<meta http-equiv=\"content-type\" content=\"text/html;charset=UTF-8\" />\n");
    outputStream << QString::fromUtf8("</head>\n<body>\n<h1>");
    outputStream << d->sourceObject->property("emptyHoursCounter").toLongLong();
    outputStream << QString::fromUtf8("</h1>\n</body>\n</html>\n");
    updateRrd();
}

void Logger::currentElectricCurrentHasChanged()
{
    QFile outputFile(QString::fromUtf8("/www/pages/currentElectricCurrent.html"));
    QTextStream outputStream(&outputFile);
    outputFile.open(QFile::WriteOnly);

    outputStream << QString::fromUtf8("<!DOCTYPE html>\n<html>\n<head>\n");
    outputStream << QString::fromUtf8("<meta http-equiv=\"content-type\" content=\"text/html;charset=UTF-8\" />\n");
    outputStream << QString::fromUtf8("</head>\n<body>\n<h1>");
    outputStream << d->sourceObject->property("currentElectricCurrent").toInt();
    outputStream << QString::fromUtf8("</h1>\n</body>\n</html>\n");
    updateRrd();
}

void Logger::currentPowerUsageHasChanged()
{
    QFile outputFile(QString::fromUtf8("/www/pages/currentPowerUsage.html"));
    QTextStream outputStream(&outputFile);
    outputFile.open(QFile::WriteOnly);

    outputStream << QString::fromUtf8("<!DOCTYPE html>\n<html>\n<head>\n");
    outputStream << QString::fromUtf8("<meta http-equiv=\"content-type\" content=\"text/html;charset=UTF-8\" />\n");
    outputStream << QString::fromUtf8("</head>\n<body>\n<h1>");
    outputStream << d->sourceObject->property("currentPowerUsage").toInt();
    outputStream << QString::fromUtf8("</h1>\n</body>\n</html>\n");
    updateRrd();
}

#include "logger.moc"

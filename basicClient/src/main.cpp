#include "logger.h"
#include "remoteinformationinterface.h"

#include <QtDBus/QDBusConnection>
#include <QtCore/QCoreApplication>

int main(int argc, char *argv[])
{
    QCoreApplication theApp(argc, argv);

    org::mgallien::RemoteInformation *theInterface = new org::mgallien::RemoteInformation(QString::fromUtf8("org.mgallien.RemoteInformation"), QString::fromUtf8("/RemoteInformation"), QDBusConnection::systemBus());

    Logger theLogger(theInterface);

    theApp.connect(theInterface, SIGNAL(hourTypeChanged()), &theLogger, SLOT(hourTypeHasChanged()));
    theApp.connect(theInterface, SIGNAL(emptyHoursCounterChanged()), &theLogger, SLOT(emptyHoursCounterHasChanged()));
    theApp.connect(theInterface, SIGNAL(fullHoursCounterChanged()), &theLogger, SLOT(fullHoursCounterHasChanged()));
    theApp.connect(theInterface, SIGNAL(currentElectricCurrentChanged()), &theLogger, SLOT(currentElectricCurrentHasChanged()));
    theApp.connect(theInterface, SIGNAL(currentPowerUsageChanged()), &theLogger, SLOT(currentPowerUsageHasChanged()));

    return theApp.exec();
}

# Copyright 2012 Matthieu Gallien <matthieu_gallien@yahoo.fr>
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 2 of
# the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

include_directories(${CMAKE_CURRENT_BINARY_DIR})
include_directories(${CMAKE_CURRENT_SOURCE_DIR})

set(readInfoBasicClient_SOURCES
    main.cpp
    logger.cpp
)

qt4_add_dbus_interfaces(readInfoBasicClient_SOURCES ../../daemon/src/org.mgallien.RemoteInformation.xml)

qt4_automoc(${readInfoBasicClient_SOURCES})

add_executable(readInfoClient ${readInfoBasicClient_SOURCES})

target_link_libraries(readInfoClient
    ${QT_QTCORE_LIBRARY}
    ${QT_QTDBUS_LIBRARY}
)

install(TARGETS readInfoClient ${INSTALL_TARGETS_DEFAULT_ARGS})


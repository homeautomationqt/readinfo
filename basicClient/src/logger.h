/*
 * Copyright 2012 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#if !defined LOGGER_H_
#define LOGGER_H_

#include <QtCore/QObject>

class LoggerPrivate;

class Logger : public QObject
{
    Q_OBJECT
public:

    Logger(QObject *sourceObject, QObject *parent = 0);

public Q_SLOTS:

    void hourTypeHasChanged();

    void fullHoursCounterHasChanged();

    void emptyHoursCounterHasChanged();

    void currentElectricCurrentHasChanged();

    void currentPowerUsageHasChanged();

private:

    void updateRrd();

    LoggerPrivate *d;
};

#endif

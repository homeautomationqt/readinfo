/*
 * Copyright 2012 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QtJolie/Client>
#include <QtJolie/Message>
#include <QtJolie/PendingReply>
#include <QtJolie/Value>

#include <QtCore/QCoreApplication>
#include <QtCore/QDebug>

Jolie::Value getPropertyValue(Jolie::Client &theClient, const QByteArray &name)
{
    Jolie::Message theMsg("/", "getProperty");
    theMsg.setData(Jolie::Value(name));

    Jolie::Message answerMsg(theClient.call(theMsg));
    if (answerMsg.isValid()) {
        qDebug() << "valid reply";
        if (answerMsg.data().isValid()) {
            qDebug() << "valid data";
            if (answerMsg.data().isInt()) {
                qDebug() << "value:" << answerMsg.data().toInt();
                return answerMsg.data();
            }
            if (answerMsg.data().isDouble()) {
                qDebug() << "value:" << answerMsg.data().toDouble();
                return answerMsg.data();
            }
            if (answerMsg.data().isByteArray()) {
                qDebug() << "value:" << answerMsg.data().toByteArray();
                return answerMsg.data();
            }
        }
    }
    return Jolie::Value();
}

int main(int argc, char *argv[])
{
    QCoreApplication theApp(argc, argv);
    Jolie::Client theClient(QString::fromUtf8("at91sam9x5ek"), 1234);

    Jolie::Message theMsg("/", "properties");
    Jolie::Message answerMsg(theClient.call(theMsg));
    if (answerMsg.isValid()) {
        qDebug() << "valid reply";
        if (!answerMsg.data().children("properties").empty()) {
            qDebug() << "at least one property";
            auto propertyList = answerMsg.data().children("properties");
            for (auto itProperty = propertyList.begin(); itProperty != propertyList.end(); ++itProperty) {
                if (itProperty->isValid() && itProperty->isByteArray()) {
                    qDebug() << "property name:" << itProperty->toByteArray();
                }
                getPropertyValue(theClient, itProperty->toByteArray());
            }
        }
    }

    Jolie::Message theNotifyMsg("/", "notifyChange");
    theMsg.setData(Jolie::Value("currentElectricCurrent"));
    theMsg.data().children("currentValue").push_back(getPropertyValue(theClient, "currentElectricCurrent"));
    theClient.call(theMsg);
}
